# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests
import base64
import hashlib
import json
import os
import random

from PIL import Image
from io import BytesIO

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

from challenge.libs import attachdiff
from com.serializers import PredictionSerializer

ALGO_TOKEN = 'simDcapLUQmHq9hv3P6ILOEKuNv1'

CLOTHING_LABELS = [
    'button down shirt',
    'running shoes',
    'socks',
    'shoe',
    'tunic',
    'sneakers',
    'pants suit formal',
    'lightweight jacket',
    'blazer',
    'backpack or messenger bag',
    'casual dress',
    'sweatshirt',
    'formal dress',
    'glasses',
    'hat',
    'denim jacket',
    'boots',
    'blouse',
    'rompers',
    'dress',
    'top-shirt',
    'top handle bag',
    'suit',
    'vest',
    'skirt',
    'sandals',
    'pants',
    'lingerie',
    'shorts',
    'jacket-coat',
    'belts',
    'pants casual',
    'heels pumps or wedges',
    'shoulder bag',
    'headwrap',
    'sweatpants',
    'trenchcoat',
    'gloves',
    'clutches',
    'jeans',
    'leggings',
    'overall',
    'sunglasses',
    'watches',
    'hosiery',
    'underwear',
    't shirt',
    'sweater',
    'leather jacket',
    'romper',
    'jumpsuit',
    'tank top',
    'winter jacket',
    'sweater dress',
    'bag',
    'swimwear',
    'jewelry',
    'flats'
]


# Create your views here.
def index(request):
    return render(request, 'index.html')


class AlgoAPIView(APIView):
    def post(self, request, format=None):
        url = 'https://api.algorithmia.com/v1/web/algo/algorithmiahq/DeepFashion/1.2.2'
        headers = {
            'DNT': '1',
            'Authorization': 'Simple {}'.format(ALGO_TOKEN),
            'Accept': 'application/json, text/javascript',
            'Origin': 'https://demos.algorithmia.com/',
            'Referrer': 'https://demos.algorithmia.com/deep-fashion/',
            'Content-Type': 'application/json',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
        }
        image_url = request.data.get('url')
        response = requests.post(url, data=json.dumps(image_url), headers=headers)
        json_response = response.json()

        img_response = requests.get(image_url)
        img = Image.open(BytesIO(img_response.content))

        articles = json_response['result']['articles']

        existing_files = os.listdir(settings.MEDIA_ROOT)
        segmented_imgs = []
        i = 0
        m = hashlib.sha256()

        # for each prediction, crop img
        for item in articles:
            i += 1
            label = item['article_name']
            bbox = item['bounding_box']
            text = '{}{}{}'.format(url, label, i).encode('utf-8')
            m.update(text)
            href = '{}${}.png'.format(label, m.hexdigest()[:6])

            if href in existing_files:
                print("not making new file, already exists")
            else:
                # bounding box
                x0 = bbox['x0']
                x1 = bbox['x1']
                y0 = bbox['y0']
                y1 = bbox['y1']

                print("{},{},{},{}".format(x0, x1, y0, y1))

                buffered = BytesIO()
                # left, upper, right, lower
                cropped_image = img.crop(
                    (
                        x0,
                        y0,
                        x1,
                        y1
                    )
                )
                cropped_image.save(buffered, format="PNG")
                cropped_image.save('{}/{}'.format(settings.MEDIA_ROOT, href), format="PNG")

            segmented_imgs.append({
                'href': href,
                'label_name': label,
            })

        return Response(segmented_imgs)


class ImageAPIView(APIView):
    def post(self, request, format=None):
        serialized_data = PredictionSerializer(request.data).data
        url = serialized_data['url']
        predicted_items = serialized_data['data']['data']['outputs'][0]['labels']['predicted']
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))

        segmented_imgs = []

        # request data
        print(predicted_items)

        existing_files = os.listdir(settings.MEDIA_ROOT)
        i = 0
        m = hashlib.sha256()
        # for each prediction, crop img
        for item in predicted_items:
            i += 1
            label = item['label_name']
            bbox = item['roi']['bbox']

            text = '{}{}{}'.format(url, label, i).encode('utf-8')
            m.update(text)
            href = '{}${}.png'.format(label, m.hexdigest()[:6])

            if href in existing_files:
                print("not making new file, already exists")
            else:
                # bounding box
                xmin = bbox['xmin']
                xmax = bbox['xmax']
                ymin = bbox['ymin']
                ymax = bbox['ymax']

                img_width = img.size[0]
                img_height = img.size[1]

                left = img_width * xmin
                top = img_height * ymin
                right = img_width * xmax
                bottom = img_height * ymax

                print("{},{},{},{}".format(left, top, right, bottom))

                buffered = BytesIO()
                # left, upper, right, lower
                cropped_image = img.crop(
                    (
                        img_width * xmin,
                        img_height * ymin,
                        img_width * xmax,
                        img_height * ymax
                    )
                )
                cropped_image.save(buffered, format="PNG")
                cropped_image.save('{}/{}'.format(settings.MEDIA_ROOT, href), format="PNG")

            segmented_imgs.append({
                'href': href,
                'label_name': label,
            })

        return Response(segmented_imgs)


class ImageComparisonAPIView(APIView):
    def get(self, request, format=None):
        query_params = request.query_params
        filename = query_params.get('filename')
        recommendations_dir = os.path.join(settings.MEDIA_ROOT, 'recommendations')

        other_files = []
        rec_files = os.listdir(recommendations_dir)
        for fname in rec_files:
            path = os.path.join(recommendations_dir, fname)
            if os.path.isdir(path) or fname == filename:
                continue
            other_files.append('{}/{}'.format('recommendations', fname))

        # other_files = list(filter(ignore_file_and_dirs, os.listdir(recommendations_dir)))[:13]

        print("other_files sent = ", other_files)
        print("num files sent = ", len(other_files))
        # call function here
        segmented_imgs = attachdiff(other_files, [filename])
        filtered_segmented = list(filter(lambda x: x['diff'] < 90, segmented_imgs))

        for segments in filtered_segmented:
            basename = os.path.basename(segments['file'])
            name, price, store, _ = basename.split('$')
            segments['name'] = name
            segments['price'] = price
            segments['store'] = store

        print("segmented_imgs = ", filtered_segmented)
        print("num filtered_segmented = ", len(filtered_segmented))
        return Response(filtered_segmented)
