from django.conf.urls import url

from com import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^segment/$', views.ImageAPIView.as_view(), name='segment'),
    url(r'^algo/$', views.AlgoAPIView.as_view(), name='algo'),
    url(r'^compare/$', views.ImageComparisonAPIView.as_view(), name='compare'),
]
