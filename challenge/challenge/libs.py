# ---------------------------------------------------------------------------
from collections import namedtuple
from math import sqrt
import random
from django.conf import settings
import os

try:
    import Image
except ImportError:
    from PIL import Image

Point = namedtuple('Point', ('coords', 'n', 'ct'))
Cluster = namedtuple('Cluster', ('points', 'center', 'n'))


def get_points(img):
    points = []
    w, h = img.size
    for count, color in img.getcolors(w * h):
        points.append(Point(color, 3, count))
    return points


rtoh = lambda rgb: '#%s' % ''.join(('%02x' % p for p in rgb))


def colorz(filename, n=3):
    img = Image.open('{}/{}'.format(settings.MEDIA_ROOT, filename))
    print('img-->', img, '  type-->', type(img))
    img.thumbnail((200, 200))
    w, h = img.size
    points = get_points(img)
    clusters = kmeans(points, n, 1)
    rgbs = [map(int, c.center.coords) for c in clusters]
    return map(rtoh, rgbs)


def euclidean(p1, p2):
    return sqrt(sum([
        (p1.coords[i] - p2.coords[i]) ** 2 for i in range(p1.n)
    ]))


def calculate_center(points, n):
    vals = [0.0 for i in range(n)]
    plen = 0
    for p in points:
        plen += p.ct
        for i in range(n):
            vals[i] += (p.coords[i] * p.ct)
    return Point([(v / plen) for v in vals], n, 1)


def kmeans(points, k, min_diff):
    clusters = [Cluster([p], p, p.n) for p in random.sample(points, k)]
    while 1:
        plists = [[] for i in range(k)]
        for p in points:
            smallest_distance = float('Inf')
            for i in range(k):
                distance = euclidean(p, clusters[i].center)
                if distance < smallest_distance:
                    smallest_distance = distance
                    idx = i
            plists[idx].append(p)
        diff = 0
        for i in range(k):
            old = clusters[i]
            center = calculate_center(plists[i], old.n)
            new = Cluster(plists[i], center, old.n)
            clusters[i] = new
            diff = max(diff, euclidean(old.center, new.center))
        if diff < min_diff:
            break
    return clusters


# color=list(colorz(imgurl,1))

# --------------------------------------------------------------------------------

# requirements
# colormapeasydev

import colormap
import colorsys
from colormap import rgb2hex
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000


# hexval=rgb2hex(0, 128, 64)

def RGBtoHEX(RGB):
    R = RGB[0]
    G = RGB[1]
    B = RGB[2]
    HEX = rgb2hex(R, G, B)
    return HEX


def HEXtoRGB(HEX):
    h = HEX.lstrip('#')
    RGB = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
    return RGB


def HSVtoRGB(HSV):
    H = RGB[0]
    S = RGB[1]
    V = RGB[2]
    return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h, s, v))


def RGBtoHSV(RGB):
    R = RGB[0]
    G = RGB[1]
    B = RGB[2]
    HSV = colorsys.rgb_to_hsv(R, G, B)
    return HSV


def comparecolors(imgHEX_1, imgHEX_2):
    print('----imgHEX_1-------', imgHEX_1)
    c1 = list(colorz(imgHEX_1, 1))[0]
    print('------------c1------------>', c1, '---type--->', type(c1))
    imgRGB_1 = HEXtoRGB(c1)
    print('----imgHEX_2-------', imgHEX_2)
    c2 = colorz(imgHEX_2, 1)
    print('------------c2------------>', c2, '---type--->', type(c2))
    imgRGB_2 = HEXtoRGB(list(colorz(imgHEX_2, 1))[0])
    imgRGB_1P = sRGBColor(imgRGB_1[0], imgRGB_1[1], imgRGB_1[2])
    imgRGB_2P = sRGBColor(imgRGB_2[0], imgRGB_2[1], imgRGB_2[2])
    # Convert from RGB to Lab Color Space
    imgLAB_1 = convert_color(imgRGB_1P, LabColor)
    # Convert from RGB to Lab Color Space
    imgLAB_2 = convert_color(imgRGB_2P, LabColor)
    # Find the color difference
    delta_e = delta_e_cie2000(imgLAB_1, imgLAB_2)
    return delta_e


surls = ['u1', 'u2', 'u3', 'u4']


# simgs=['i1','i2','i3','i4']

def _splitem(alist, sep="$"):
    newlist = []
    for i in alist:
        a = i.split(sep)
        newlist.append(a)
    print('-------split OK------------')
    return newlist


def shortlist(longlist, item):
    '''takes long list of all files and filters to finds everything but the item we are finding matching outfits for'''
    longlistNames = _splitem(longlist)
    itemNames = _splitem(item)
    shortlist = []
    for n, i in zip(longlistNames, range(len(longlistNames))):
        if n != item:
            shortlist.append(longlistNames[i])
    print('-------shortlist OK------------')
    return shortlist


def attachdiff(sl_uri, INPUT_SELECTED_IMAGE_URI):
    '''takes list of urls, item and returns list of dicts with uri and diff'''
    difflist = []
    for u in sl_uri:
        print('-----u----->', u, '-----INPUT_SELECTED_IMAGE_URI----->', INPUT_SELECTED_IMAGE_URI)
        diff = comparecolors(u, INPUT_SELECTED_IMAGE_URI[0])
        print('diffcompare success')
        diffdict = {}
        diffdict.update({'file': u, 'diff': diff})
        difflist.append(diffdict)
    return difflist

# PAYLOAD = attachdiff()
